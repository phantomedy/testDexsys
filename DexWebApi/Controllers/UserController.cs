﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using DexWebApi.Models;
using DexWebApi.Models.LiteDto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DexWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly AplContext _db;

        public UserController(AplContext context)
        {
            _db = context;
        }

        [HttpGet]
        public IEnumerable<UserLiteDto> Get()
        {
           return _db.Users.GroupJoin(
                _db.Purchaseses,
                user => user.Id,
                purchases => purchases.UserId,
                (user, puLst) => new UserLiteDto
                {
                    Id = user.Id,
                    Name = user.Name,
                    Family = user.Family,
                    Otchestvo = user.Otchestvo,
                    MobileNumber = user.MobileNumber,
                    Email = user.Email,
                    Avatar = user.Avatar,
                    DatePurchases = user.Purchaseses.OrderByDescending(pu => pu.DatePurchase).FirstOrDefault().DatePurchase.ToShortDateString()
                });
        }

        [HttpGet("{id}")]
        public UserLiteDto Get(int id)
        {
            var purchasesesLite = _db.Purchaseses
                .GroupJoin(_db.PurchasedProductses, pu => pu.Id, pp => pp.PurchasesId, (pu, ppLst) => new {pu, ppLst})
                .Select(x => new PurchaseLiteDto()
                {
                    Id = x.pu.Id,
                    DatePurchase = x.pu.DatePurchase.ToShortDateString(),
                    NumberPurchase = x.pu.NumberPurchase,
                    UserId = x.pu.UserId,
                    StatePurchases = _db.StatePurchaseses.Where(s => s.Id == x.pu.StatePurchasesId).Select(s => s.State).FirstOrDefault(),
                    SumPrice = Math.Round(x.ppLst.GroupJoin(_db.Products, pp => pp.ProductId, p => p.Id, (pp, p) => p.Sum(t => t.Price * pp.CountProducts)).Sum(t => t),2),
                }).Where(u => u.UserId == id).OrderByDescending(x => x.DatePurchase).ToList();

            var user = _db.Users.Where(x => x.Id == id).Select(x => new UserLiteDto()
            {
                Id = x.Id,
                Name = x.Name,
                Family = x.Family,
                Otchestvo = x.Otchestvo,
                RegisterDate = x.RegisterDate.ToShortDateString(),
                MobileNumber = x.MobileNumber,
                Avatar = x.Avatar,
                Email = x.Email,
                PurchaseLiteDtos = purchasesesLite
            }).FirstOrDefault(u => u.Id == id);

            return user;
        }

        [HttpPost]
        public IActionResult Post([FromBody]User user)
        {         
            if (ModelState.IsValid)
            {
                user.RegisterDate = DateTime.Now;
                _db.Users.Add(user);
                _db.SaveChanges();
                return Ok(user);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]UserLiteDto user)
        {
            User castUser = new User
            {
                Id = user.Id,
                Avatar = user.Avatar,
                Name = user.Name,
                Family = user.Family,
                Otchestvo = user.Otchestvo,
                MobileNumber = user.MobileNumber,
                Email = user.Email,
                RegisterDate = Convert.ToDateTime(user.RegisterDate)
            };

            if (ModelState.IsValid)
            {
                _db.Update(castUser);
                _db.SaveChanges();
                return Ok(castUser);
            }
            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            User user = _db.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
            {
                _db.Users.Remove(user);
                _db.SaveChanges();
            }
            return Ok(user);
        }
    }
}