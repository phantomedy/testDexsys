﻿using System;
using System.Collections.Generic;
using System.Linq;
using DexWebApi.Models;

namespace DexWebApi
{
    public static class InitialEntityInDb
    {
        public static void CreateUsers(AplContext db)
        {
            if (db.Users.Any()) return;

            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/43.jpg",
            //    Name = "Иван",
            //    Family = "Крутихин",
            //    Otchestvo = "Алексеевич",
            //    MobileNumber = 1234567890,
            //    Email = "Ivan@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/3.jpg",
            //    Name = "Петр",
            //    Family = "Дорошенко",
            //    Otchestvo = "Сергеевич",
            //    MobileNumber = 9876543210,
            //    Email = "Petya@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/18.jpg",
            //    Name = "Сергей",
            //    Family = "Кузьминых",
            //    Otchestvo = "Петрович",
            //    MobileNumber = 5432167890,
            //    Email = "Sergey@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/women/10.jpg",
            //    Name = "Джесика",
            //    Family = "Альба",
            //    Otchestvo = "Петровна",
            //    MobileNumber = 6454646464,
            //    Email = "Djesika@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/women/57.jpg",
            //    Name = "Елена",
            //    Family = "Попова",
            //    Otchestvo = "Игоревна",
            //    MobileNumber = 7557646676,
            //    Email = "Helen@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/80.jpg",
            //    Name = "Григорий",
            //    Family = "Штольц",
            //    Otchestvo = "Алексеевич",
            //    MobileNumber = 4869956336,
            //    Email = "Grisha@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/women/66.jpg",
            //    Name = "Екатерина",
            //    Family = "Богатырева",
            //    Otchestvo = "Николаевна",
            //    MobileNumber = 7575578394,
            //    Email = "Katya@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/81.jpg",
            //    Name = "Константин",
            //    Family = "Короваев",
            //    Otchestvo = "Сергеевич",
            //    MobileNumber = 5735389575,
            //    Email = "Kostya@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/61.jpg",
            //    Name = "Станислав",
            //    Family = "Собин",
            //    Otchestvo = "Игоревич",
            //    MobileNumber = 6464783794,
            //    Email = "Stas@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/women/68.jpg",
            //    Name = "Юлия",
            //    Family = "Петренко",
            //    Otchestvo = "Николаевна",
            //    MobileNumber = 3457867306,
            //    Email = "Ylya@mail.ru"
            //});
            //db.Users.Add(new User
            //{
            //    Avatar = "https://randomuser.me/api/portraits/men/40.jpg",
            //    Name = "Евгений",
            //    Family = "Шляпин",
            //    Otchestvo = "Константинович",
            //    MobileNumber = 765656464,
            //    Email = "Evgeny@mail.ru"
            //});
            //db.SaveChanges();
        }

        public static void Init(AplContext db)
        {
            if (db.Users.Any() || db.Products.Any() || db.StatePurchaseses.Any()) return;

            var u1 = new User
            {
                Avatar = "https://randomuser.me/api/portraits/men/43.jpg",
                Name = "Иван",
                Family = "Крутихин",
                Otchestvo = "Алексеевич",
                MobileNumber = "8-(123)-456-78-90",
                Email = "Ivan@mail.ru"
            };
            var u2 = new User
            {
                Avatar = "https://randomuser.me/api/portraits/men/3.jpg",
                Name = "Петр",
                Family = "Дорошенко",
                Otchestvo = "Сергеевич",
                MobileNumber = "8-(987)-654-32-10",
                Email = "Petya@mail.ru"
            };

            var p1 = new Product {Name = "Kartoha", Price = 20.5};
            var p2 = new Product {Name = "Luk", Price = 5.6};
            var p3 = new Product {Name = "Morkov", Price = 17.9};
            var p4 = new Product {Name = "Tomat", Price = 65.4};
            var p5 = new Product {Name = "Ogurec", Price = 45.7};

            var st1 = new StatePurchases {State = "В обработке"};
            var st2 = new StatePurchases {State = "Ожидает оплаты"};
            var st3 = new StatePurchases {State = "Оплачен"};
            var st4 = new StatePurchases {State = "Отменен"};


            db.Users.AddRange(new List<User> {u1, u2});
            db.Products.AddRange(new List<Product> {p1, p2, p3, p4, p5});
            db.StatePurchaseses.AddRange(new List<StatePurchases> {st1, st2, st3, st4});
            db.SaveChanges();


            var rnd = new Random();

            var pu1 = new Purchases
            {
                UserId = u1.Id,
                NumberPurchase = rnd.Next(1, 99999),
                DatePurchase = Convert.ToDateTime("01/01/2018"),
                StatePurchasesId = st3.Id
            };
            var pu2 = new Purchases
            {
                UserId = u1.Id,
                NumberPurchase = rnd.Next(1, 99999),
                DatePurchase = Convert.ToDateTime("12/05/2018"),
                StatePurchasesId = st1.Id
            };

            var pu3 = new Purchases
            {
                UserId = u2.Id,
                NumberPurchase = rnd.Next(1, 99999),
                DatePurchase = Convert.ToDateTime("12/09/2018"),
                StatePurchasesId = st1.Id
            };

            db.Purchaseses.AddRange(new List<Purchases> {pu1, pu2, pu3});
            db.SaveChanges();

            var pp1 = new PurchasedProducts
            {
                PurchasesId = pu1.Id,
                ProductId = p1.Id,
                CountProducts = 2
            };

            var pp2 = new PurchasedProducts
            {
                PurchasesId = pu1.Id,
                ProductId = p3.Id,
                CountProducts = 3
            };

            var pp3 = new PurchasedProducts
            {
                PurchasesId = pu2.Id,
                ProductId = p4.Id,
                CountProducts = 3
            };

            var pp4 = new PurchasedProducts
            {
                PurchasesId = pu3.Id,
                ProductId = p4.Id,
                CountProducts = 7
            };

            db.PurchasedProductses.AddRange(new List<PurchasedProducts>{pp1,pp2,pp3,pp4});

            db.SaveChanges();
        }
    }
}
