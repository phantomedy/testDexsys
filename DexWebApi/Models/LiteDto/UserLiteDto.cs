﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DexWebApi.Models.LiteDto
{
    public class UserLiteDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Otchestvo { get; set; }
        public string Avatar { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string DatePurchases { get; set; }
        public string RegisterDate { get; set; }
        public List<PurchaseLiteDto> PurchaseLiteDtos { get; set; }
    }
}
