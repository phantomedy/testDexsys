﻿using System;
using System.Collections.Generic;

namespace DexWebApi.Models.LiteDto
{
    public class PurchaseLiteDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int NumberPurchase { get; set; }
        public string DatePurchase { get; set; }
        public string StatePurchases { get; set; }
        public double SumPrice { get; set; }
    }
}
