﻿using System;
using System.Collections.Generic;

namespace DexWebApi.Models
{
    public class Purchases
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int NumberPurchase { get; set; }
        public DateTime DatePurchase { get; set; }
        public int StatePurchasesId { get; set; }
        public StatePurchases StatePurchases { get; set; }
        public List<PurchasedProducts> PurchasedProductses { get; set; }

        public Purchases()
        {
            PurchasedProductses = new List<PurchasedProducts>();
        }
    }
}
