﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DexWebApi.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Укажите имя пользователя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Укажите фамилию пользователя")]
        public string Family { get; set; }

        [Required(ErrorMessage = "Укажите отчество пользователя")]
        public string Otchestvo { get; set; }

        public string Avatar { get; set; }


        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public DateTime RegisterDate { get; set; }
        public List<Purchases> Purchaseses { get; set; }

        public User()
        {
            Purchaseses = new List<Purchases>();
        }
    }
}
