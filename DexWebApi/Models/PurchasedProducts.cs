﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DexWebApi.Models
{
    public class PurchasedProducts
    {
        public int Id { get; set; }
        public int PurchasesId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int CountProducts { get; set; }
    }
}
