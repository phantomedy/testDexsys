﻿using System;
using Microsoft.EntityFrameworkCore;

namespace DexWebApi.Models
{
    public class AplContext : DbContext
    {
        public AplContext(DbContextOptions<AplContext> options) : base(options) {}

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Purchases> Purchaseses { get; set; }
        public DbSet<PurchasedProducts> PurchasedProductses { get; set; }
        public DbSet<StatePurchases> StatePurchaseses { get; set; }
    }
}
