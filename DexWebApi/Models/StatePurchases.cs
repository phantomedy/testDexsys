﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DexWebApi.Models
{
    public class StatePurchases
    {
        public int Id { get; set; }
        public string State { get; set; }
    }
}
