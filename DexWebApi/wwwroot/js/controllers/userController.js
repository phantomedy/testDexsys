(function() {
    angular.module('Dexsys').controller('userController',
        function ($scope, $http, userService, paginationService, fileReader) {

            var url = "api/User";

            $scope.imageSrc = "";

            $scope.visibility = false;

            // ��������� ������ �������������
            userService.getUsers()
                .then(
                    function success(value) {
                        $scope.users = value;
                        paginationService.setItems(value, 5);
                        $scope.pagination = paginationService.getPagination();
                    },
                    function error(response) {
                        alert(response.status);
                    });

            $scope.addUser = function () {
                $scope.stateView = "AddUser";
                $scope.user = null;
            },
                $scope.editUser = function (u) {
                    $scope.user = u;
                    $scope.stateView = "EditUser";
                },
                $scope.returnUsersCards = function () {
                    $scope.stateView = "";
                },
                $scope.getUser = function (id) {
                    $http.get(url + "/" + id)
                        .then(
                        function success(response) {
                            $scope.user = response.data;
                            $scope.stateView = "ViewUser";
                        },
                        function error(response) {
                            alert(response.status);
                        });
                },
                $scope.saveUser = function (id, user, form) {
                    if (form.$valid) {
                        if (angular.isUndefined(id)) {

                            $http.post(url, user)
                                .then(
                                    function success(response) {
                                        $scope.users.push(response.data);
                                        $scope.stateView = "";
                                    },
                                    function error(response) {
                                        alert(response.status);
                                    });

                        } else {

                            $http.put(url + "/" + id, user)
                                .then(
                                    function success(response) {
                                        userService.getUsers().then(
                                            function(value) {
                                                $scope.users = value;
                                            });
                                        $scope.stateView = "";
                                    },
                                    function error(response) {
                                        alert(response.status);
                                    });

                        }
                    }
                },

                $scope.tempId = function(id) {
                    $scope.userId = id;
                }

                $scope.delUser = function() {
                    $http.delete(url + "/" + this.userId)
                        .then(function success(response) {
                            userService.getUsers().then(
                                function(value) {
                                    $scope.users = value;
                                });
                        });
                },

                // ������������ �������
                $scope.showPage = function(page) {

                    if (angular.isUndefined(page)) {
                        paginationService.setCurrentPage('all');
                        $scope.pagination = paginationService.getPagination();
                    } else {
                        paginationService.setCurrentPage(page);
                        $scope.pagination = paginationService.getPagination();
                    }
                }         
        });
}())