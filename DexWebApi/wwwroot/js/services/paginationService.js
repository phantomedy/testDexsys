angular.module('Dexsys').service('paginationService', function(){
    var currentPage = 1,
        itemsOnPage = 3,
        items = []; 
    return {

        // ��������� ��������� ����������
        setItems: function(data, perPage){
            items = data;
            itemsOnPage = angular.isUndefined(perPage) ? itemsOnPage : perPage;
        },

        // ��������� ������� ��������
        setCurrentPage: function(page){
            currentPage = page;
        },

        // ��������� ���-�� �������
        getCountPages: function(){
            return Math.ceil(items.length / itemsOnPage);
        },

        // ��������� ������� � ���-��� �������
        getPaginationList: function(){
            var countPages = this.getCountPages(),           
                pagination = [];
            if(countPages > 1){
                for(var i = 1; i <= countPages; i++){
                    pagination.push(i);
                }
            }
            return pagination;
        },

        // ��������� ������� ��������� 
        getPagination: function(){
            return{                         
                start: currentPage == 'all' ? 0 : (currentPage - 1) * itemsOnPage,
                pagination: this.getPaginationList(),
                currentPage: currentPage,
                itemsOnPage: currentPage == 'all' ? items.length : itemsOnPage
            }           
        }
    };
});