angular.module('Dexsys').service("userService", function ($http, $q) {
    return {
        getUsers: function () {
            var deferred = $q.defer();
            $http.get("api/User")
                .then(
                    function success(response) {
                        deferred.resolve(response.data);
                    },
                    function error(response) {
                        deferred.reject(response.status);
                });
            return deferred.promise;
        }
    };
});