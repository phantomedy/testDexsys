(function(){
    angular.module('Dexsys').directive('paginationDir', function(){
        return {
          restrict: 'E',
          templateUrl: 'js/templates/pagination.html'
        };
    });
}())