(function(){
    angular.module('Dexsys').directive('userAdd', function(){
        return {
          restrict: 'E',
          templateUrl: 'js/templates/userAdd.html'
        };
    });
}())