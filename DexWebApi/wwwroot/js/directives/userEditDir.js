(function(){
    angular.module('Dexsys').directive('userEdit', function(){
        return {
          restrict: 'E',
          templateUrl: 'js/templates/userEdit.html'
        };
    });
}())