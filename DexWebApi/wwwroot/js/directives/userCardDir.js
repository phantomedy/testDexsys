(function(){
    angular.module('Dexsys').directive('userCard', function(){
        return {
          restrict: 'E',
          templateUrl: 'js/templates/userCard.html'
        };
    });
}())
