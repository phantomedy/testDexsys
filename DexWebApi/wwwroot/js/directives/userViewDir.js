(function(){
    angular.module('Dexsys').directive('userView', function(){
        return {
          restrict: 'E',
          templateUrl: 'js/templates/userView.html'
        };
    });
}())