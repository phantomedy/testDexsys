﻿SET IDENTITY_INSERT [dbo].[Users] ON
insert into Users (Id,Name,Family,Otchestvo,Avatar,MobileNumber,Email,RegisterDate) values (1,N'Иван', N'Крутихин', N'Алексеевич', N'https://randomuser.me/api/portraits/men/43.jpg', N'8-(123)-122-78-90', N'Ivan@mail.ru', '2018-05-21')
insert into Users (Id,Name,Family,Otchestvo,Avatar,MobileNumber,Email,RegisterDate) values (2,N'Петр', N'Дорошенко', N'Сергеевич', N'https://randomuser.me/api/portraits/men/3.jpg', N'8-(123)-411-14-00', N'Petya@mail.ru', '2018-05-15')
insert into Users (Id,Name,Family,Otchestvo,Avatar,MobileNumber,Email,RegisterDate) values (3,N'Юлия', N'Петренко', N'Николаевна', N'https://randomuser.me/api/portraits/women/68.jpg', N'8-(123)-556-48-54', N'Ylya@mail.ru', '2018-05-10')
insert into Users (Id,Name,Family,Otchestvo,Avatar,MobileNumber,Email,RegisterDate) values (4,N'Екатерина', N'Богатырева', N'Николаевна', N'https://randomuser.me/api/portraits/women/66.jpg', N'8-(123)-455-45-52', N'Katya@mail.ru', '2018-05-01')
insert into Users (Id,Name,Family,Otchestvo,Avatar,MobileNumber,Email,RegisterDate) values (5,N'Джесика', N'Альба', N'Петровна', N'https://randomuser.me/api/portraits/women/10.jpg', N'8-(123)-852-11-96', N'Djesika@mail.ru', '2018-05-03')
SET IDENTITY_INSERT [dbo].[Users] OFF

SET IDENTITY_INSERT [dbo].[Products] ON
INSERT INTO Products (Id, Name, Price) VALUES (1, N'Kartoha', 20.5)
INSERT INTO Products (Id, Name, Price) VALUES (2, N'Luk', 5.6)
INSERT INTO Products (Id, Name, Price) VALUES (3, N'Morkov', 17.9)
INSERT INTO Products (Id, Name, Price) VALUES (4, N'Tomat', 65.4)
INSERT INTO Products (Id, Name, Price) VALUES (5, N'Ogurec', 45.7)
SET IDENTITY_INSERT [dbo].[Products] OFF

SET IDENTITY_INSERT [dbo].[StatePurchaseses] ON
INSERT INTO StatePurchaseses (Id, State) VALUES (1, N'В обработке')
INSERT INTO StatePurchaseses (Id, State) VALUES (2, N'Ожидает оплаты')
INSERT INTO StatePurchaseses (Id, State) VALUES (3, N'Оплачен')
INSERT INTO StatePurchaseses (Id, State) VALUES (4, N'Отменен')
SET IDENTITY_INSERT [dbo].[StatePurchaseses] OFF

SET IDENTITY_INSERT [dbo].[Purchaseses] ON
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (1, 1111, '2018-06-01', 3, 1)
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (2, 2222, '2018-06-02', 1, 1)

insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (3, 3333, '2018-06-03', 3, 2)
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (4, 4444, '2018-06-04', 1, 2)

insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (5, 5555, '2018-06-05', 3, 3)
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (6, 6666, '2018-06-06', 1, 3)
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (7, 1212, '2018-06-11', 4, 3)

insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (8, 7777, '2018-06-07', 3, 4)
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (9, 8888, '2018-06-08', 1, 4)

insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (10, 9999, '2018-06-09', 3, 5)
insert into Purchaseses (Id,NumberPurchase,DatePurchase,StatePurchasesId,UserId) values (11, 1112, '2018-06-10', 1, 5)
SET IDENTITY_INSERT [dbo].[Purchaseses] OFF

SET IDENTITY_INSERT [dbo].[PurchasedProductses] ON
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (1,1,1,3)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (2,1,2,2)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (3,2,3,4)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (4,3,1,2)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (5,3,3,1)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (6,3,2,3)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (7,4,5,1)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (8,4,2,4)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (9,5,5,1)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (10,6,1,1)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (11,6,4,1)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (12,7,1,2)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (13,8,2,3)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (14,8,5,1)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (15,8,3,1)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (16,9,1,3)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (17,10,2,1)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (18,10,4,1)

insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (19,11,1,1)
insert into PurchasedProductses (Id,PurchasesId,ProductId,CountProducts) values (20,11,3,1)
SET IDENTITY_INSERT [dbo].[PurchasedProductses] OFF